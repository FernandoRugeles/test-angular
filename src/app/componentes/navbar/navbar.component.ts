import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Util } from 'src/app/helpers/Util';
import { Usuario } from 'src/app/views/modelos/Usuario';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  util: Util;
  usuarioSesion: Usuario;

  constructor(util: Util, private router: Router) {
    this.util = util;
    this.usuarioSesion = new Usuario();
  }

  ngOnInit(): void {

  }

  ngDoCheck() {
    const res = this.util.getSesionXItem('usuarioSesion');
    this.usuarioSesion = JSON.parse(res);
  }

  salir() {
    this.util.limpiarSesion();
    this.router.navigate(['login']);
  }

  editar() {
    this.router.navigate(['editar']);
  }

}
