import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CargandoService } from 'src/app/servicios/cargando/cargando.service';

@Component({
  selector: 'app-cargando',
  templateUrl: './cargando.component.html',
  styleUrls: ['./cargando.component.css']
})
export class CargandoComponent implements OnInit {

  cargando: boolean = false;
  cargandoSubscription!: Subscription;

  constructor(private cargandoService: CargandoService) { }

  ngOnInit() {
    this.cargandoSubscription = this.cargandoService.estado.subscribe((value) => {
      this.cargando = value;
    });
  }

  ngOnDestroy() {
    this.cargandoSubscription.unsubscribe();
  }

}
