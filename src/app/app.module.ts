import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './views/login/login.component';
import { PokedexListadoComponent } from './views/pokedex-listado/pokedex-listado.component';
import { CargandoComponent } from './componentes/cargando/cargando.component';
import { PokedexService } from './servicios/pokedex/pokedex.service';
import { ErrorsHandler } from './servicios/error/errors-handler.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { CargandoInterceptor } from './servicios/interceptor/cargando.interceptor';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';
import { SnackBar } from './servicios/error/SnackBar';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogoComponent } from './componentes/dialogo/dialogo.component';
import { RegistrarseComponent } from './views/registrarse/registrarse.component';
import { Util } from './helpers/Util';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { LoginGuard } from './servicios/login.guard';
import { PokedexDetalleComponent } from './views/pokedex-detalle/pokedex-detalle.component';
import { EditarUsuarioComponent } from './views/editar-usuario/editar-usuario.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PokedexListadoComponent,
    CargandoComponent,
    DialogoComponent,
    RegistrarseComponent,
    NavbarComponent,
    PokedexDetalleComponent,
    EditarUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatIconModule,
    MatToolbarModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    PokedexService,
    ErrorsHandler,
    MatSnackBar,
    Overlay,
    SnackBar,
    Util,
    LoginGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CargandoInterceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogoComponent]
})
export class AppModule { }
