import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokedexDetalleComponent } from './pokedex-detalle.component';

describe('PokedexDetalleComponent', () => {
  let component: PokedexDetalleComponent;
  let fixture: ComponentFixture<PokedexDetalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokedexDetalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokedexDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
