import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from '../modelos/Pokemon';
import { Util } from 'src/app/helpers/Util';
import { PokedexService } from 'src/app/servicios/pokedex/pokedex.service';

@Component({
  selector: 'app-pokedex-detalle',
  templateUrl: './pokedex-detalle.component.html',
  styleUrls: ['./pokedex-detalle.component.css']
})
export class PokedexDetalleComponent implements OnInit {

  pokemon: Pokemon;

  constructor(private util: Util, private servicio: PokedexService, private router: Router) {
    const data: Pokemon = JSON.parse(this.util.getSesionXItem('detallePokemon'));
    this.pokemon = new Pokemon(data.id, data.height, data.weight, data.name, { front_default: data.sprites.front_default }, data.types, data.moves, []);
  }

  volver(){
    this.router.navigate(['pokedexListado']);
  }

  ngOnInit(): void {
    this.servicio.obtenerEvolucion(this.pokemon.id).subscribe((res) => {
      let obj = this.servicio.crearCadenaEvolucion(res.chain);
      this.pokemon.evolution = obj;
    })
  }

}
