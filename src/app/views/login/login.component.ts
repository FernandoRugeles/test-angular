import { Component, OnInit } from '@angular/core';
import { Usuario } from '../modelos/Usuario';
import { Util } from '../../helpers/Util';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  objeto: Usuario;
  formulario: FormGroup;

  constructor(private router: Router, private util: Util, private formBuilder: FormBuilder) {
    this.objeto = new Usuario();
    this.formulario = formBuilder.group({
      usuario: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      clave: ['', [Validators.required, Validators.minLength(8)]],
    })
  }

  ngOnInit(): void {

  }

  onSubmit() {
    if (!this.formulario.valid) {
      alert('debe llenar el formulario');
      return;
    }
    this.objeto.usuario = this.formulario.get('usuario')?.value;
    //nombre aignado para tenas de prueba
    this.objeto.nombre = 'Fernando Rugeles'
    this.util.agregarSesionXItem('usuarioSesion', this.objeto);
    this.router.navigate(['pokedexListado']);
  }

  registrarse() {
    this.router.navigate(['registrarse']);
  }

  ngDoCheck() {
    this.util.limpiarSesion();
  }

  get usuario() {
    return this.formulario.get('usuario')
  }

  get clave() {
    return this.formulario.get('clave')
  }

}
