import { Component, HostListener, OnInit } from '@angular/core';
import { PokedexService } from 'src/app/servicios/pokedex/pokedex.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogoComponent } from 'src/app/componentes/dialogo/dialogo.component';
import { Router } from '@angular/router';
import { Util } from 'src/app/helpers/Util';
import { Pokemon } from '../modelos/Pokemon';

@Component({
  selector: 'app-pokedex-listado',
  templateUrl: './pokedex-listado.component.html',
  styleUrls: ['./pokedex-listado.component.css']
})
export class PokedexListadoComponent implements OnInit {

  pagina: number;
  resultado: Array<any>;
  resultadoFiltrado: Array<any>;
  totalLista: number;
  filtro: string;
  limite: number;
  offset: number;

  constructor(private util: Util, private router: Router, private servicio: PokedexService) {
    this.pagina = 0;
    this.limite = 50;
    this.resultado = [];
    this.resultadoFiltrado = [];
    this.totalLista = 0;
    this.filtro = '';
    this.offset = 0;
  }

  ngOnInit(): void {
    this.consultar();
  }

  consultar() {
    this.pagina = this.pagina + 1;
    this.servicio.consultar(this.offset, this.limite).subscribe(
      (res: any) => {
        this.totalLista = res.count;
        res.results.forEach((resultado: any) => {
          this.servicio.obtenerDetalles(resultado.name).subscribe((res) => {
            let obj = res;
            obj.types = this.servicio.crearTipos(obj.types);
            this.resultado.push(res);
            this.filtrar();
          })
        });
        this.offset = this.offset + this.limite;
      }
    )
  }

  filtrar() {
    var pattern = this.filtro.split("").map((x) => {
      return `(?=.*${x})`
    }).join("");
    var regex = new RegExp(`${pattern}`, "g")
    const filtrado = this.resultado.filter(val => {
      return regex.test(val.name) || regex.test(val.types)
    });

    this.resultadoFiltrado = filtrado.sort((a, b) => a < b ? -1 : 1);
  }

  abrirDialogo(pokemon: Pokemon) {
    this.util.agregarSesionXItem('detallePokemon', pokemon);
    this.router.navigate(['pokedexDetalle']);
  }

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event: any) {
    if (window.innerHeight + window.scrollY === document.body.scrollHeight) {
      this.consultar();
    }
  }
}
