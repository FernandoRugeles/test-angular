import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokedexListadoComponent } from './pokedex-listado.component';

describe('PokedexListadoComponent', () => {
  let component: PokedexListadoComponent;
  let fixture: ComponentFixture<PokedexListadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokedexListadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokedexListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
