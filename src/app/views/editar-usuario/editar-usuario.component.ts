import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Util } from 'src/app/helpers/Util';
import { Usuario } from '../modelos/Usuario';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {
  usuarioSesion: Usuario;
  formulario: FormGroup;

  constructor(private router: Router, private util: Util, private formBuilder: FormBuilder) {
    const res = this.util.getSesionXItem('usuarioSesion');
    this.usuarioSesion = JSON.parse(res);

    this.formulario = formBuilder.group({
      usuario: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      nombre: ['', [Validators.required]]
    })
  }

  onSubmit(){
    if (!this.formulario.valid) {
      alert('debe llenar el formulario');
      return;
    }
    this.usuarioSesion.usuario = this.formulario.get('usuario')?.value;
    this.usuarioSesion.nombre = this.formulario.get('nombre')?.value;
    this.util.agregarSesionXItem('usuarioSesion', this.usuarioSesion);
    this.router.navigate(['pokedexListado']);
  }

  ngOnInit(): void {
  }

  get usuario() {
    return this.formulario.get('usuario')
  }

  get nombre() {
    return this.formulario.get('nombre')
  }

}
