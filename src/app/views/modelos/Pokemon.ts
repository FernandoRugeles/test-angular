interface sprites {
    front_default: string
}

interface move {
    name: string
}

interface listMoves {
    move: move
}

interface evolucion {
    name: string
}

export class Pokemon {
    id: number;
    height: number;
    weight: number;
    name: string;
    sprites: sprites;
    types: string;
    moves: Array<listMoves>;
    evolution: Array<evolucion>;

    constructor(id: number, height: number, weight: number, name: string, sprites: sprites, types: string, moves: Array<listMoves>, evolution: Array<evolucion>) {
        this.id = id;
        this.height = height;
        this.weight = weight;
        this.name = name;
        this.sprites = sprites;
        this.types = types;
        this.moves = moves;
        this.evolution = evolution;
    }
}