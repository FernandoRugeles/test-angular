import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Util } from 'src/app/helpers/Util';
import { Usuario } from '../modelos/Usuario';

@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.component.html',
  styleUrls: ['./registrarse.component.css']
})
export class RegistrarseComponent implements OnInit {

  objeto: Usuario;
  formulario: FormGroup;

  constructor(private router: Router, private util: Util, private formBuilder: FormBuilder) {
    this.formulario = formBuilder.group({
      usuario: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      clave: ['', [Validators.required, Validators.minLength(8)]],
      confirmarClave: ['', [Validators.required, Validators.minLength(8)]],
      nombre: ['', [Validators.required]]
    }, {
      validator: [
        this.util.MustMatch('clave', 'confirmarClave'),
        this.util.validarMinimoDosMayusculas('clave'),
        this.util.validarMinimoDosMayusculas('confirmarClave'),
        this.util.validarSiTieneCaracteresEspeciales('clave'),
        this.util.validarSiTieneCaracteresEspeciales('confirmarClave'),
        this.util.validarALMenosUnNumero('clave'),
        this.util.validarALMenosUnNumero('confirmarClave')
      ]
    })

    this.objeto = new Usuario();
    this.util.agregarSesionXItem('usuarioSesion', new Usuario());
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (!this.formulario.valid) {
      alert('debe llenar el formulario');
      return;
    }
    this.objeto.usuario = this.formulario.get('usuario')?.value;
    this.objeto.nombre = this.formulario.get('nombre')?.value;
    this.util.agregarSesionXItem('usuarioSesion', this.objeto);
    this.router.navigate(['pokedexListado']);
  }

  ingresar() {
    this.router.navigate(['login']);
  }

  get usuario() {
    return this.formulario.get('usuario')
  }

  get clave() {
    return this.formulario.get('clave')
  }

  get confirmarClave() {
    return this.formulario.get('confirmarClave')
  }

  get nombre() {
    return this.formulario.get('nombre')
  }
}
