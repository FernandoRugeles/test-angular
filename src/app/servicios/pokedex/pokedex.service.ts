import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import config from '../../config/config.json';
import { ErrorsHandler } from '../error/errors-handler.service';
import { catchError } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Observable, map, switchMap, forkJoin, tap } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  result: any = [];
  apiUrl: string = `${(<any>config).rutaServicio}`;

  constructor(private handleError: ErrorsHandler, private translate: TranslateService, private httpClient: HttpClient, private _snackBar: MatSnackBar) {

  }

  consultar(offset: number, limite: number): Observable<Array<any>> {
    return this.httpClient.get<any>(`${this.apiUrl}pokemon?offset=${offset}&limit=${limite}`).pipe(
      catchError(this.handleError.handleError)
    );
  }

  obtenerDetalles(nombre: string): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}pokemon/${nombre}/`).pipe(
      catchError(this.handleError.handleError)
    );
  }

  obtenerEvolucion(id: number): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}evolution-chain/${id}/`).pipe(
      catchError(this.handleError.handleError)
    );
  }

  crearTipos(tipos: any) {
    return tipos.map((t: any) => { return t.type.name }).toString();
  }

  crearCadenaEvolucion(evoluciones: any) {
    let detalle = evoluciones['evolves_to'];
    detalle.map((d: any) => {
      this.result.push({
        "name": d.species.name,
      });
      if (d.evolves_to) {
        this.crearCadenaEvolucion(d);
      }
    })
    return this.result;
  }
}
