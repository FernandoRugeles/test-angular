import { Injectable, Injector } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SnackBar {

    public subj_notification: Subject<string> = new Subject();

    durationInSeconds: number;
    constructor(public _snackBar: MatSnackBar, private injector: Injector) {
        this.durationInSeconds = 5;
    }

    lanzarError(mensajeUno: string, mensajeDos: string) {
        this._snackBar.open(mensajeUno, mensajeDos, {
            duration: this.durationInSeconds * 1000,
        });
    }

}