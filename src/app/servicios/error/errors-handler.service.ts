import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

import { MatSnackBar } from '@angular/material/snack-bar';
import '../../../assets/js/notificaciones';
import '../../../assets/js/modal-notify';
declare var notificacion: any;
declare var modalNotify: any;
import config from '../../config/config.json';

@Injectable()
export class ErrorsHandler implements ErrorHandler {

	snackbar!: MatSnackBar;
	durationInSeconds = 5;

	constructor(private readonly msb: MatSnackBar, private readonly zone: NgZone) { }

	handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			/*this.snackbar.open('A ocurrido un error:', error.error.message, {
				duration: this.durationInSeconds * 1000,
			});*/
			console.error(error.error.message);
		} else {
			/*this.snackbar.open('A ocurrido un error:', error.error != undefined ? error.message : 'Error no manejado', {
				duration: this.durationInSeconds * 1000,
			});*/
			console.error(error.error != undefined ? error.message : 'Error no manejado');
		}
		if ((<any>config).showErrorBack) {
			/*this.snackbar.open('A ocurrido un error:', error.error, {
				duration: this.durationInSeconds * 1000,
			});*/
			console.error(error.error);
		}
		return throwError('A ocurrido un error. intente denuevo mas tarde, si el error persiste porfavor comunicarse con el administrador del sistema');
	}

}