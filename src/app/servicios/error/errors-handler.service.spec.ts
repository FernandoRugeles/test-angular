import { TestBed } from '@angular/core/testing';

import { ErrorsHandler } from './errors-handler.service';

describe('ErrorsHandler', () => {
  let service: ErrorsHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorsHandler);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
