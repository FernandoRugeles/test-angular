import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { CargandoService } from "../cargando/cargando.service";
import { finalize } from "rxjs/operators";


@Injectable()
export class CargandoInterceptor implements HttpInterceptor {

    activeRequests: number = 0;

    skippUrls = [
        '/authrefresh',
    ];

    constructor(private cargandoService: CargandoService) {
        cargandoService.iniciarCarga();
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let displayLoadingScreen = true;
        for (const skippUrl of this.skippUrls) {
            if (new RegExp(skippUrl).test(request.url)) {
                displayLoadingScreen = false;
                break;
            }
        }

        if (displayLoadingScreen && request.url.indexOf('autocomplete') < 0) {
            if (this.activeRequests === 0) {
                this.cargandoService.iniciarCarga();
            }
            this.activeRequests++;

            return next.handle(request).pipe(
                finalize(() => {
                    this.activeRequests--;
                    if (this.activeRequests === 0) {
                        this.cargandoService.pararCarga();
                    }
                })
            )
        } else {
            return next.handle(request);
        }
    };

}
