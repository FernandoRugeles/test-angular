import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CargandoService {

  private _cargando: boolean = false;
  estado: Subject<any> = new Subject();

  get cargando(): boolean {
    return this._cargando;
  }

  set cargando(value) {
    this._cargando = value;
    this.estado.next(value);
  }

  iniciarCarga() {
    this.cargando = true;
  }

  pararCarga() {
    this.cargando = false;
  }

}
