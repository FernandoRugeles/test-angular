import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Util } from '../helpers/Util';
import { Usuario } from '../views/modelos/Usuario';

@Injectable()
export class LoginGuard implements CanActivate {
    usuario: any;
    util: Util;

    constructor(util: Util, private router: Router) {
        this.util = util;
    }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        let url = this.util.url();
        if(url.includes('login') || url.includes('registrarse')){
            return true;
        }
        let sesion: Usuario = JSON.parse(this.util.getSesionXItem('usuarioSesion'));
        if (!sesion?.nombre) {
            alert('debe iniciar sesion');
            this.router.navigate(['login'])
            return false;
        }
        return true;
    }
}
