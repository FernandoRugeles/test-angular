
import { IfStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router'
import { Usuario } from '../views/modelos/Usuario';

@Injectable()
export class Util {
  constructor(private router: Router) {

  };

  limpiarSesion() {
    localStorage.removeItem('usuarioSesion');
    return true;
  };

  getSesionXItem(item: string): any {
    return localStorage.getItem(item);
  };

  agregarSesionXItem(llave: string, valor: any) {
    localStorage.setItem(llave, JSON.stringify(valor));
    return true;
  };

  url() {
    return this.router.url;
  }

  validarSiTieneCaracteresEspeciales(controlName: string) {
    return (formGroup: FormGroup) => {
      const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
      const control = formGroup.controls[controlName];
      if (control.errors && !control.errors.validarSiTieneCaracteresEspeciales) {
        return;
      }

      if (!specialChars.test(control.value)) {
        control.setErrors({ ValidarSiTieneCaracteresEspeciales: true });
      } else {
        control.setErrors(null);
      }
    }
  }

  validarMinimoDosMayusculas(controlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];

      if (control.errors && !control.errors.validarMinimoDosMayuculas) {
        return;
      }

      let contadorControl: number = 0;
      control.value.split('').map((letra: string) => {
        if (isNaN(Number(letra)) && letra.charAt(0).toUpperCase() === letra.charAt(0)) {
          contadorControl = contadorControl + 1;
        }
      })

      if (contadorControl < 2) {
        control.setErrors({ ValidarMinimoDosMayuculas: true });
      } else {
        control.setErrors(null);
      }
    }
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ MustMach: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  validarALMenosUnNumero(controlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];

      if (control.errors && !control.errors.validarALMenosUnNumero) {
        return;
      }

      let contadorControl: number = 0;
      control.value.split('').map((letra: string) => {
        if (!isNaN(Number(letra))) {
          contadorControl = contadorControl + 1;
        }
      })

      if (contadorControl < 1) {
        control.setErrors({ ValidarALMenosUnNumero: true });
      } else {
        control.setErrors(null);
      }
    }
  }
}
