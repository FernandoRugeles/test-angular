import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { RegistrarseComponent } from './views/registrarse/registrarse.component';
import { PokedexListadoComponent } from './views/pokedex-listado/pokedex-listado.component';
import { LoginGuard } from './servicios/login.guard';
import { PokedexDetalleComponent } from './views/pokedex-detalle/pokedex-detalle.component';
import { EditarUsuarioComponent } from './views/editar-usuario/editar-usuario.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'registrarse', component: RegistrarseComponent },
  { path: 'pokedexListado', component: PokedexListadoComponent, canActivate: [LoginGuard] },
  { path: 'pokedexDetalle', component: PokedexDetalleComponent, canActivate: [LoginGuard] },
  { path: 'editar', component: EditarUsuarioComponent, canActivate: [LoginGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
