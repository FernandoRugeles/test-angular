var modalNotify = (function () {
    return{
        show: function(html){
            $("#modal-notify").addClass("fadeIn animated").html(html).fadeIn().dblclick(function(){
                $(this).hide();
            });
        }
    }
})(modalNotify || {});
