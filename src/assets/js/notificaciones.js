var notificacion = (function () {
    return {
        success: function (objeto) {
            var notify = $.notify({
                // options
                icon: 'fa fa-thumbs-o-up fa-lg',
                title: objeto.title,
                message: objeto.message,
                url: objeto.url,
                target: '_blank'
            }, {
                    // settings
                    type: 'success',
                    allow_dismiss: objeto.closable,
                    timer: objeto.timer,
                    placement: {
                        from: objeto.from,
                        align: objeto.aling
                    }
                });
        },
        error: function (objeto) {
            var notify = $.notify({
                // options
                icon: 'fa fa-exclamation-circle fa-lg',
                title: objeto.title,
                message: objeto.message,
                url: objeto.url,
                target: '_blank',
            }, {
                    // settings
                    type: 'danger',
                    allow_dismiss: objeto.closable,
                    timer: objeto.timer,
                    placement: {
                        from: objeto.from,
                        align: objeto.aling
                    }
                }, );
        },
        warning: function (objeto) {
            var notify = $.notify({
                // options
                icon: 'fa fa-exclamation-triangle fa-lg',
                title: objeto.title,
                message: objeto.message,
                url: objeto.url,
                target: '_blank'
            }, {
                    // settings
                    type: 'warning',
                    allow_dismiss: objeto.closable,
                    timer: objeto.timer,
                    placement: {
                        from: objeto.from,
                        align: objeto.aling
                    }
                });
        },
        info: function (objeto) {
            var notify = $.notify({
                // options
                icon: 'fa fa-exclamation fa-lg',
                title: objeto.title,
                message: objeto.message,
                url: objeto.url,
                target: '_blank'
            }, {
                    // settings
                    type: 'info',
                    allow_dismiss: objeto.closable,
                    timer: objeto.timer,
                    placement: {
                        from: objeto.from,
                        align: objeto.aling
                    }
                });
        },
    }

})(notificacion || {})